#¿Qué es un podcast?
En esta guía de Radios Libres vas a aprender sobre este formato radiófonico diital:
¿Qué es un podcast y cuáles son sus particularidades?, ¿Es cualquier audio subido a la web?,
¿Una radio que cuelga sus radiorevistas online está haciendo podcast?

##Un poco de historia

Desde los inicios de la web han existido en Internet páginas con archivos en ogg, mp3 u otros formatos de audio comprimido para descargarlos y escucharlos, pero no se llamaban podcast.

Todo indica que en 2004 el periodista británico Ben Hammersley unió por primera vez la palabra iPod y broadcasting (radiodifusión) en un artículo en The Guardian hablando sobre las posibilidades y el auge de la “radio amateur”.

*“Reproductores de Mp3, como el iPod de Apple, en muchos bolsillos, el software de producción de audio barato o libre, y el movimiento blogger como parte importante de Internet; estos son los ingredientes para un nuevo boom de la radio amateur. Pero, ¿cómo podemos llamar a esto? ¿Audioblogging?, ¿Podcasting? ¿Guerrilla Media?” Leer el artículo original en https://www.theguardian.com/*

Otros sugieren que se deriva de la suma de palabras pod (portable) + broadcast. También está la versión que afirma que deriva de Portable On Demand + Broadcast, es decir, emisión portátil bajo demanda.

Controversias aparte, el podcast tuvo su época dorada alrededor de 2004. Tal fue el boom de la palabrita que el New Oxford American Dictionary seleccionó podcast como la Palabra del Año 2005 y la definió como: *“una grabación digital de una emisión de radio o un programa similar, que se puede obtener de Internet para ser descargado en un reproductor de audio personal”.*

Básicamente, el *podcasting* es eso, colocar archivos de audio en nuestra web. La mayor ventaja del podcast es que permite decidir al oyente cuándo escuchar sus programas preferidos sin la esclavitud de un horario. Una especie de “radio a la carta” donde elegir los platos que más le gustan de una variado menú radiofónico.

Pero **no cualquier programa subido a una página web es un podcast.** Para que el podcast sea estrictamente un podcast debería estar sindicado. Descifremos esta otra palabreja.

**Sindicación web (feed)** se refiere a un sistema de notificación web al que te puedes suscribir. Por ejemplo, te gusta un programa de una determinada radio o sus noticias. En vez de entrar a la web todos los días y descargar el audio, éste sistema permite que te instales un software (se les conoce como agregadores), te suscribas al programa y cada vez que se publica un podcast nuevo el software lo descarga por ti. Los formatos más conocidos de sindicación web son Atom y RSS. Seguro que han visto muchas veces el símbolo de la sindicación en varias webs.

![](images/icon-rss.png)
