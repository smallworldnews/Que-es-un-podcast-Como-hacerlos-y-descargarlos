#Cómo descargar un podcast
Como acabamos de comentar se hace a través de un agragador, que puede ser un software, un navegador web, un gestor de correo, una página web o una app para el celular.

Las **apps móviles** se pueden instalar desde la página de los mismos productores de los podcast, aunque no todos las tienen disponibles. El inconveniente es que tendrías que instalar una app por cada uno de los podcast que quieras descargar. Las principales plataformas web para alojar podcast de forma gratuita también ofrecen una app para el celular con la que te puedes suscribir a los diferentes canales de esa plataforma.

Los **navegadores o gestores de correo** también sirven como agregadores. Puedes probar cómo se ve el canal de RSS de Radios Libres https://radioslibres.net/rss/ en el navegador (tienes que verlo en **Firefox**, ya que Chrome ya no da soporte para este formato).

![](images/como-descargar-podcast-navegador1.png)


Y puedes suscribirte a través del gestor de correos Thunderbird.

![](images/como-descargar-podcast-thunderbird2.png)

Canal de RSS en el agregador del gestor de correo. Como ves permite hasta escuchar los audios directamente si el reproductor es compatible (HTML5)

Si prefieres un **software** te recomendamos gPodder. Hay versiones para todos los sistemas operativos, incluído Android para que escuches tus podcast desde el móvil. Y si te inclinas por una aplicación en la web Feedly es buena opción.

![](images/como-descargar-podcast-gpodder3.png)


Así se ve el podcast Radio Liberada en gPodder.
