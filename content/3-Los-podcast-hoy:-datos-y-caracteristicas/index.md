# Los podcast hoy: datos y características
Más de diez años después los podcast vuelven a estar de moda. Nunca se fueron, pero resurgen con fuerza en una época donde el acceso a la Red ha crecido y los medios tradicionales siguen explorando cómo fusionarse con Internet. Grandes cadenas de medios apuestan ahora por este formato, como la Radio Pública de Estados Unidos (NPR), la cadena Ser en España con su plataforma Podium Podcast o en Argentina Posta FM.

Aunque el estudio del Estado de los Medios 2016 realizado por Pew Research Center analiza sólo el comportamiento de los medios en Estados Unidos nos sirve para sondear la salud del formato. Segun los resultados de la investigación *“1 de cada 5 personas escuchó algún tipo de podcast en el último mes, frente al 12% hace seis años”.* Aunque aún es un número minoritario, se han duplicado en estos seis últimos años.

![](images/podcast-datos-crecimiento1.png)


## Crecimiento de los podcast en Estados Unidos. Pew Research Center.

El estudio también recoge datos de los escuchas de la radio en línea, que se ha duplicado desde 2010 pasando del 27% al 57% lo que no significa que hasta haga mermar los escuchas de la radio tradicional que sigue rozando la totalidad de la población: *“el 91% de personas mayores de 12 años escuchó radio terrestre en el último mes”.*

Este nuevo revivir de los podcast ya no se fija tanto en si los programas están o no sindicados. La mayoría de nuevos podcast suelen tener estas características:

- Son programas producidos especialmente para ser podcast. Es decir, no se hacen para una radio determinada se transmiten y luego se cuelgan en la web. **Se piensan y graban para Internet,** para que estén alojados en la Red. Eso no quita que puedan transmitirse después por una radio, bien FM o radio en línea, pero el orden es primero Internet y luego la transmisión, no al revés.
- **Las lógicas de tiempo y producción** son de “enlatado”. No superan los 60 minutos, aunque la mayoría no pasa de 30 minutos. Son más elaborados, ya que al no salir en vivo hay mayor tiempo para la edición por lo que se emplean mayores recursos técnicos como efectos, variedad de cortinas,…
- Muchos de quienes los producen **no son radialistas habituales** ni trabajan en una radio. Pueden ser aficionados a los videojuegos que graban un podcast sobre esa temática.
- **Son series de podcast** no programas sueltos y, por lo general, se agrupan en temporadas, como las series de televisión. En esto es interesante destacar como muchos de los programas apuestan por los podcast **dramatizados y de ficción** al más puro estilo de las tradicionales radionovelas.
- Tienen una **regularidad.** Los oyentes los esperan un día determinado cada semana, quincenal o mensuales.
- Existe entre ellos una **unidad temática.** Hay podcast específicos de literatura, de software libre, sobre comunicación, determinados tipos de música,…
- Suelen ser **atemporales.** Es decir, tratan dinámicas fuera de la coyuntura, la mayoría no habla sobre noticias de actualidad. No quita que existan algunos con esa temática, pero son los menos. Es cierto que para hablar de un determinado tema, por ejemplo software libre de automatización radial, pueden tomar como excusa el lanzamiento de un nuevo programa, pero en realidad el programa no se ciñe al evento en sí, sino al tema.
- Una gran mayoría **usa licencias libres** para publicar sus programas lo que permite que otros medios los difundan. Si sólo permites que tus podcast los escuchen en tu plataforma llegará a mucha menos audiencia que si los publicas en abierto y otras radios los retransmiten.
