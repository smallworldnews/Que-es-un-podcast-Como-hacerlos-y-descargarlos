#Cómo hacer un podcast

Con los podcast la producción de radio se universalizó, se hizo accesible a la ciudadanía. Es muy sencillo y barato. Va una breve guía de cómo hacerlo.

**1. Graba el audio**
Es imprescindible contar con un pequeño estudio de producción, tan pequeño que basta con un micrófono y una computadora con su correspondiente tarjeta de sonido. Debes contar también con un editor de audio instalado en la computadora. Recomendamos **Audacity** por su sencillez y versatilidad. Si quieres algo con más posibilidades prueba **Ardour.**

Es necesario que guardes el audio comprimido en ogg o mp3. Recuerda que el podcast estará alojado en una web y, de ser muy pesado, quienes visiten tu página tardarán mucho tiempo en bajarlo.

**2. Bautiza tu podcast**
Aunque en la web coloques el título y el autor del podcast es necesario que el mismo audio vaya “bautizado”. De esta manera, al sonarlo en cualquier computadora o reproductor portátil se podrán leer los datos.

Los audios digitales permiten colocar dentro de sus bits la información del podcast. Esto se hace editando las *tags* de los metadatos. Estas etiquetas ―eso significa tag― se pueden colocar con casi todos los software reproductores de audio o programas específicos para ello. El editor Audacity lo permite cuando guardas el audio.

![](images/como-hacer-podcast-metatags1.png)


**3. Publica el podcast**
Para compartir el programa necesitas alojarlo en el ciberespacio. Tienes dos opciones. Una, contar con página web propia o un blog que permita alojar audios. En este caso, todo se facilita. Sólo tienes que subir el audio y enlazarlo en la página.

La segunda, la más frecuente, es que tengas un blog sin espacio virtual para alojar audios. Necesitarás un servidor gratuito donde subir el audio, para después publicarlo en tu blog. Recomendamos http://radioteca.net o http://archive.org

**4. Sindica tu podcast**
La mayoría de blogs ya generan el feed automáticamente y sólo tienes que colocar ese enlace en algún espacio visible de tu sitio. También las principales plataformas de alojamiento de podcast proveen un enlace de suscripción (Feed RSS). Otra opción es usar algún software como PodcastGenerator o similares.
